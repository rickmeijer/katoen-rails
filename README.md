Katoen
================================================================================

Katoen is a no-nonsense front-end framework built for agencies developers. 
It's perfectly suited for a quick one-off single-page website or as a 
foundation for something much larger. The main focus of this framework is 
to provide a system that shrinks as your project grows, so you don't have to
fight it every step of the way. As such it's primary focus is on flexibility 
and utility, and not so much as a set of predetermined and predesigned compo components.

Features:
* Lightweight. Bloat should never be an issue when considering using this
	toolset.
* Flexible. Adaptable to any design you can throw at it, and 
* Strong opinions, weakly held. It's opinionated without locking anything 
	down. If you're unsatisfied however, drop us a line and we can see if we
	can accomodate.

Installation:
--------------------------------------------------------------------------------
* Add `katoen-port-rails` to your gemfile, inside the private mangrove block.
* Optionally, run `rails generate katoen` to add a _variables.scss in your 
	app/assets/stylesheets which you can modify to suit your needs.
* Get to work!

Grid:
--------------------------------------------------------------------------------
It's a industry standard float-based layout with padding determining the 
gutters. To say the grid system was inspired by Bootstrap is an understatement.
There are however, several enhanchements that set it apart.

* Default width of 100%; no need to specify probable default behaviour.
* Variable gutter width
* Not neccesarily mobile-first, but built for it by default. 
* Able to render as placeholder selectors if you're not keen on using 
	utility classes in your layouts.
* No pull- push- or offsetcolumns. It's usually more efficient to rethink
	your approach and use ```right``` or ```center``` class.

##On using offset/push/pull columns:
There are no built-in push- pull- or offset columns. You can change your
approach to the layout, and not need another ~400 lines of code.
To build a simple two-column layout where the navigation has a one column 
offset for example you can do this:

```html
<div class="container">
	<div class="row">
		<aside class="sm-3">Navigation</aside>
		<section class="sm-8 right">Content</section>
	</div>
</div>
```
For centering columns you don't need to calculate your offset. Just juse 
```center``` and you're good to go.

9 times out of 10 you want stacking if the coloumns don't fit. All the 
columns are 100% wide by default;  you'll only have to document the changes.
So ```lg-6``` will be 100% on ```xs```, ```sm``` and ```md```. It will 
become 50% from ```lg``` onward.


Utilities
--------------------------------------------------------------------------------
These classes help you quickly do often needed jobs. Also; if your 
```$debug-level``` is bigger than 0 it will display the current breakpoint 
at the bottom right.

| Classname    				| Function
|:--------------			|:---------
| equalheight 				| Stretches the content vertically so everything is aligned. Requires modernizr or ```flexbox``` parent class
| hidetext 					| Hides tekst without needing to wrap. Handy for social buttons
| center 					| If you need a centered element this will probably do it.
| list-reset 				| Handy if you want to clear the default ul styling
| cf, clearfix 				| If your parent container doesn't expand properly because you are using floats outside of a ```container``` or ```row```
| text-left/right/center 	| Handy for quickly positioning text, images or other non-floating objects.
| no-padding 				| Hard resets padding to 0
| no-margin 				| Hard resets margin to 0
| left, right 				| Floats something or right
| clear-left/right/both 	| Clears indicated position
| circle 					| Gives border radius of 100%
| .xs/sm/md/lg/xl-hide 		| Hides on xs/sm/lg/xl+, shows on previous breakpoints. A md-hide will be hidden from md-forward
| .xs/sm/md/lg/xl-show 		| Show on xs/sm/lg/xl+, hides on previous breakpoints. A md-show will be hidden from anything below md, and shown on anything above md


Functions
--------------------------------------------------------------------------------
###cl($color, $map:$colors);
Cl is the color function. It retreives a color based on the key you pass. 
By default this is the ```$colors``` variable, which contains a map with 
keys and hexes. If you want to pass another colormap you can send it using 
the second argument.

The following turns the text grey:
```
//_variables.scss
$colors: (
    'grey': #ccc
    ... //other colors
);
//_typography.scss
body {
    color: cl('grey');
}
```

###em($unit, $parent_unit: $document_unit)
This dynamically calculates the em based on the ```$default-font-size```. If 
you're trying to emmify a parent element you can pass it into this function as
a second argument. Take note though: this is static, since it's calculated on 
compile time. Changing the ```$default-font-size``` will not change the displayed 
font-size for elements that make use of this function, since the result will 
get changed as well. 

###map-key-index
returns the index of a given key.
```sass 
$index: map-key-index($breakpoints, xs); //Returns 1
```

```str-split($string, $key); //Splits $string on $key```
Splits a string on a certain value. Handy for converting strings to maps.


Mixins
--------------------------------------------------------------------------------
###bp
One of the more powerful mixins in this framework. You can use this to add 
custom breakpoints to your code. Since everything is build to stack; passing a 
number means a media query with a min-width is set, so you can build 'mobile first'

*NOTE: You NEED a $breakpoints map with your queries defined to get the most out of
this. This comes with the grid*

```sass
//returns @media (min-width: 43.75em and max-width: 56.24em)
@include bp('md') { //code here } 

//returns @media (min-width: 43.75em)
@include bp('md+') { //code here } 
@include bp('md>') { //code here } 

//returns @media (max-width: 43.75em)
@include bp('md-') { //code here }
@include bp('md<') { //code here }

// Returns @media (min-width: 768px)
@include bp(768) { //code here } 
```

###font-scaling
The vw unit is kind of useless if we have variable content in our header. That's why 
there's a font-scaling mixin. We need a ```$breakpoints``` map in our code for 
the mixin to recognise the hierarchy in our media queries.

You can either scale linearly like so:
```sass
h1 { @include font-scaling(2em, 10em); }
```

Output:
```css
h1 { font-size: 2em; }
@media (min-width: 31.25em) { h1 { font-size: 4em; } }
@media (min-width: 43.75em) { h1 { font-size: 6em; } }
@media (min-width: 56.25em) { h1 { font-size: 8em; } }
@media (min-width: 75em) 	{ h1 { font-size: 10em; } }
```

Or you can define a font-size for each breakpoint. It's intelligent enough to
merge similar fontsizes to combat output bloat.

Input:
```sass
h1 { @include font-scaling(2em, 2em, 4em, 4em, 4em); }
```
Output:
```css
h1 { font-size: 2em; }
@media (min-width: 43.75em) { h1 { font-size: 5em; } }
```

###theming
Much like this part of the documentation, the theming mixin isn't quite yet done.
Currently it works in conjunction with a properly set up ```$theming``` map.
It accepts a @content directive, so you can add theming to the body and target
a specific class that way.

What it currently does is it loops through the $theming colormap, which specifies
primary, secondary etc colors, which you can access through the ```$theme``` map.

```sass
//input
.example {
	@include theming {
		color: cl('primary', $theme);
		background: cl('secondary', $theme);
	}
}
```

Output:
```css
.example.theme-black {
	color: #000;
	background: #fff;
}
```
### utilities
There are several mixins available for the lazy developers. ```@include hover```
adds a ```&:hover, &:focus``` on your class, for those moments you want keyboard 
accessibility for your site (which is *always*).
Or ```@include clearfix``` when you don't want or can't do anything to the DOM.


Components
--------------------------------------------------------------------------------
There are some ready made components available once you decide to use Katoen.
Because of bloat and potential styling conflicts, it's neccesary to import these
specifically to your project, if you want them.

##Spacing
<small>Add ```@import katoen/components/spacing``` to your application.scss</small>

*NOTE: I'd reccommend you to not using this because of bloat issues.
```.space-children > *``` is more efficient, easier to maintain and quicker.*

What this does is tying your gutter (which is horizontal rythm basically) 
to your vertical rythm. With the default settings you can add a ```xs-p-1``` 
to an element to give it a padding top and bottom equal to the padding on 
the left and right side. When you add ```md-p-2``` the padding will double 
on the ```md``` breakpoint. You can control the settings with the following 
variables:

```$spacing-multiplier: (2, 4);``` is an array containing the multiplications 
of the gutter. In this case; 
```xs-mt-1``` gives 2 times the gutter width, and ```xs-mt-2``` gives 4 
times the gutter width.

```$spacing-properties: (
	p: (padding-top, padding-bottom),
	mt: margin-top,
	mb: margin-bottom
);```

This determines the classname for the spacing property. The key of this sass-map 
is the second value of the classname, the first being the breakpoint. The value 
of this sass-map determines what properties you can multiply. In theory; you can
make breakpoint dependent font-sizes.

##Media
<small>Add ```@import katoen/components/media``` to your application.scss</small>
This is a standard [stubornella](http://www.stubbornella.org/content/2010/06/25/the-media-object-saves-hundreds-of-lines-of-code/) 
media object. 

html looks like this:
```html
<div class="media">
	<img src="//placehold.it/80x80" class="media__object" />
	<div class="media__content">
		Lorem ipsum
	</div>
</div>
```

##Patty
--Add ```@import katoen/components/patty``` to your application.scss--
You want an animated menu button in your project quickly? Katoen has got you
covered with a css-only solution. Just add a class ```patty``` to some element inside
the menu button. When the parent of the patty has a class of ```is-active``` the patty
will turn into a close button.