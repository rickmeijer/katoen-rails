# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'katoen/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "katoen-port-rails"
  spec.version       = Katoen::Rails::VERSION
  spec.authors       = ["Rick Meijer"]
  spec.email         = ["rickmeijer@gmail.com"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end
  
  spec.add_dependency "sass",     "~> 3.3"
  spec.add_dependency "rails",     "~> 4.2"
  
  spec.summary       = 'lightweight framework'
  spec.homepage      = "TODO: Create homepage."
  spec.license       = "MIT"

  spec.post_install_message = "Thanks for installing! Use `rails generate katoen` to set up your variables."
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
end
