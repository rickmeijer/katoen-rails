class KatoenGenerator < Rails::Generators::Base
  source_root File.expand_path("../templates", __FILE__)

  def copy_katoen_file
    copy_file  "_variables.scss", "app/assets/stylesheets/_variables.scss"
  end
end